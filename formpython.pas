unit formPython;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls;

type

  { TfrmPythonLog }

  TfrmPythonLog = class(TForm)
    btnRunCmd: TButton;
    edCommand: TComboBox;
    lbLogs: TMemo;
    Panel1: TPanel;
  private

  public

  end;

var
  frmPythonLog: TfrmPythonLog;

implementation

{$R *.lfm}

end.

