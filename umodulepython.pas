unit uModulePython;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, PythonEngine;

type

  { TModulePython }

  TModulePython = class(TDataModule)
    PythonEngine1: TPythonEngine;
    PythonInputOutput1: TPythonInputOutput;
    procedure PythonEngine1AfterInit(Sender: TObject);
    procedure PythonInputOutput1SendData(Sender: TObject; const Data: AnsiString
      );
    procedure PythonInputOutput1SendUniData(Sender: TObject;
      const Data: UnicodeString);
  private

  public
    procedure InitPython();
    procedure InitPythonModules();
    procedure PythonExamples();
    procedure AddStringToConsole(str: UTF8String);

  end;

type

  { TThreadPython }

  TThreadPython = class(TThread)
  protected
    // Main code. thread code (NO access to forms!)
    procedure Execute(); override;
  end;


var
  ModulePython: TModulePython;

function py(): TPythonEngine;

implementation

{$R *.lfm}

uses
  formPython,
  process,
  proc_py, // python
  Forms;  // Application.ExeName

const
  cPyLibraryWindows = 'python38.dll';
  cPyLibraryLinux = 'libpython3.8m.so.1.0';
  cPyLibraryMac = '/Library/Frameworks/Python.framework/Versions/3.8/lib/libpython3.8.dylib';
  cPyZipWindows = 'python38.zip';

{ TModulePython }

function py(): TPythonEngine;
begin
  Result := GetPythonEngine();
end;

{ TThreadPython }

procedure TThreadPython.Execute;
begin
  py.ExecFile('H:\Prj\OpenModbusTool\modbus_tcp_server.py');
end;

procedure TModulePython.PythonEngine1AfterInit(Sender: TObject);
var
  dir: string;
begin
  dir := ExtractFilePath(Application.ExeName);
  {$ifdef windows}
  Py_SetSysPath([
     dir+'DLLs',
     dir+cPyZipWindows,
     dir+'Lib'
     ], false);
  {$endif}
  Py_SetSysPath([dir+'Py', dir+'Lib', dir+'lib'], true);
end;

procedure TModulePython.PythonInputOutput1SendData(Sender: TObject;
  const Data: AnsiString);
begin
  AddStringToConsole('Py: '+Data);
end;

procedure TModulePython.PythonInputOutput1SendUniData(Sender: TObject;
  const Data: UnicodeString);
begin
  AddStringToConsole('Py: '+UTF8String(Data));
end;

procedure TModulePython.InitPython;
var
  S: string;
  dir: string;
begin
  dir:= ExtractFilePath(Application.ExeName);
  S:=
    {$ifdef windows} cPyLibraryWindows {$endif}
    {$ifdef linux} cPyLibraryLinux {$endif}
    {$ifdef darwin} cPyLibraryMac {$endif} ;
  PythonEngine1.DllPath:= ExtractFileDir(dir);
  PythonEngine1.DllName:= ExtractFileName(S);
  PythonEngine1.LoadDll();
end;

procedure TModulePython.InitPythonModules;
begin
  // ref: https://python-syncthing.readthedocs.io/en/latest/
  // ref: https://docs.syncthing.net/dev/rest.html

  py.PyImport_ImportModule('pymodbus');
  //py.ExecString('from syncthing import Syncthing');


  // Example:
  //py.PyImport_ImportModule('syncthing');
  //py.ExecString('from syncthing import Syncthing');
  //py.ExecString('sync = None');
end;

procedure TModulePython.PythonExamples;
begin
{
procedure TCore.PythonExecInConsole(Str: string);
begin
  AddStringToConsole('Py>: '+Str);

  if (Str<>'') and (Str[1]='=') then
    Str := 'print('+Copy(Str, 2, MaxInt) + ')';

  try
    PythonEngine1.ExecString(Str);
  except
  end;
end;

function TCore.GetConnections: PPyObject;
begin
  py.ExecString('connections = sync.system.connections()');
  Result := py.EvalString('connections');
end;

procedure TCore.AddStringToConsole(Str: UTF8String);
begin
  if frmMain.edConsole.Lines.Count > 100 then
    frmMain.edConsole.Lines.Delete(0);
  frmMain.edConsole.Lines.Add(Str);
end;

}

  {



  try
    py.ExecString('sync = Syncthing("'+self.APIKey+'")');
  except
    on e: EPythonError do
    begin
      ShowMessage('Python exception on start: '+e.Message);
    end;
  end;
  SyncthingObj := py.EvalString('sync');
  py.ExecString('print(sync)');
  py.ExecString('print(sync.system.version()["version"])');
}


  {
  var_res: Variant;
  func := PythonEngine1.PyUnicodeFromString('func_name')
  try
    var_res := PythonEngine1.EvalFunction(func, [SyncthingObj]);
  finally
    Py_DECREF(func);
  end;}

  //SyncthingObj := PythonEngine1.Run_CommandAsObject('syncthing.Syncthing('+self.APIKey+')', 0);
  //SyncthingObj := PythonEngine1.Run_CommandAsObject('Syncthing("'+self.APIKey+'")', 0);

  // получение объекта увеличивает счетчик ссылок на него,
  // чтобы его освободить нужно уменьшить счетчик ссылок.
  //NOTE: Py_XDecRef(SyncthingObj)


  {
          try
          if presult = Py_None then
            Result := 0
          else
            Result := PyObjectAsVariant( presult );
        finally
          Py_DECREF(presult);
        end;
  }
end;

procedure TModulePython.AddStringToConsole(str: UTF8String);
begin
  if frmPythonLog.lbLogs.Lines.Count > 100 then
    frmPythonLog.lbLogs.Lines.Delete(0);
  frmPythonLog.lbLogs.Lines.Add(Str);
end;

end.

